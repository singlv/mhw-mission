var Express = require('express');
var BodyParser = require('body-parser');

// CORS
var cors = require('cors');

// create express app
var app = Express();
app.use(BodyParser.urlencoded({
    extended: true
}));
app.use(BodyParser.json());

app.use(cors());

// parse requests of content-type - application/json
app.use(BodyParser.json());

// Configuring the database
var dbConfig = require('./config/database.config.js');
var mongoose = require('mongoose');
var bluebird = require('bluebird');

mongoose.Promise = bluebird;

mongoose.connect(dbConfig.url, {
    useMongoClient: true
});

mongoose.connection.on('error', function () {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});

mongoose.connection.once('open', function () {
    console.log("Successfully connected to the database");
})

// define a simple route
app.get('/', cors(), function (req, res, next) {
    res.json({
        "message": "This is api for mhw.singlv.com"
    });
    next();
});

// Require Notes routes
require('./app/routes/mission.routes.js')(app);

// listen for requests
app.listen(5000, function () {
    console.log("Server is listening on port 5000");
});