const mongoose = require('mongoose');

const MissionSchema = mongoose.Schema({
    room: String,
    hr: String,
    phr: String,
    psn: String,
    target: String,
    tag: Array,
    msg: String,
    type: String,
    difficulty: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Mission', MissionSchema);