//Start
var Mission = require('../models/mission.model.js')

exports.create = function (req, res) {
    // Create and Save a new Mission
    if (!req.body.msg) {
        res.status(400).send({
            message: "Error 400"
        });
    }
    var quest = new Mission({
        room: req.body.room || "未設定",
        hr: req.body.hr || "未設定",
        phr: req.body.phr || "未設定",
        psn: req.body.psn || "未設定",
        target: req.body.target || "未設定",
        tag: req.body.tag || "未設定",
        msg: req.body.msg || "未輸入",
        type: req.body.type || "未設定",
        difficulty: req.body.difficulty || "未設定"
    });

    quest.save(function (err, data) {
        console.log(data);
        if (err) {
            console.log(err);
            res.status(500).send({
                message: "Some error occurred while creating the mission."
            });
        } else {
            res.send(data);
        }
    });
};

exports.findAll = function (req, res) {
    // Retrieve and return all mission from the database.
    Mission.find(function (err, mission) {
        if (err) {
            res.status(500).send({
                message: "Some error occurred while retrieving mission."
            });
        } else {
            res.send(mission);
        }
    });
};

exports.findOne = function (req, res) {
    // Find a single mission with a mission id
    Mission.findById(req.params.missionId, function (err, data) {
        if (err) {
            res.status(500).send({
                message: "Could not retrieve note with id " + req.params.missionId
            });
        } else {
            res.send(data);
        }
    });
};

exports.update = function (req, res) {
    // Update a mission identified by the mission id in the request
    Mission.findById(req.params.missionId, function (err, quest) {
        if (err) {
            res.status(500).send({
                message: "Could not find a note with id " + req.params.missionId
            });
        }

        quest.room = req.body.room;
        quest.hr = req.body.hr;
        quest.phr = req.body.phr;
        quest.psn = req.body.psn;
        quest.target = req.body.target;
        quest.tag = req.body.tag;
        quest.msg = req.body.msg;
        quest.type = req.body.type;
        quest.type = req.body.difficulty;

        quest.save(function (err, data) {
            if (err) {
                res.status(500).send({
                    message: "Could not update mission with id " + req.params.missionId
                });
            } else {
                res.send(data);
            }
        });
    });
};

exports.delete = function (req, res) {
    // Delete a mission with the specified mission id in the request
    Mission.remove({
        _id: req.params.missionId
    }, function (err, data) {
        if (err) {
            res.status(500).send({
                message: "Could not delete note with id " + req.params.id
            });
        } else {
            res.send({
                message: "Note deleted successfully!"
            })
        }
    });
};