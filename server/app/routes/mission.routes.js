module.exports = function (app) {

    var mission = require('../controllers/mission.controller.js');

    // Create a new Mission
    app.post('/missions', mission.create);

    // Retrieve all Missions
    app.get('/missions', mission.findAll);

    // Retrieve a single Mission with ID
    app.get('/missions/:missionId', mission.findOne);

    // Update a Mission
    app.put('/missions/:missionId', mission.update);

    // Delete a Mission with ID
    app.delete('/missions/:missionId', mission.delete);
}