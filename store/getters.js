export const getters = {
  missionStart: state => {
    return (state.currentPage - 1) * state.itemsPerPage
  }
}
