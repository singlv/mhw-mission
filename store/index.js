import Vue from 'vue'
import Vuex from 'vuex'
import axios from '~/plugins/axios'

// import * as getters from './getters'
// import router from 'router'
// import { sync } from 'vuex-router-sync'

// Modules
// import MissionList from './modules/MissionList'

Vue.use(Vuex)

const state = {
  missions: [],
  itemsPerPage: 12,
  currentPage: 1,
  activeType: null,
  createMission: false,
  sideMenu: false
}

const getters = {
  missionStart: state => {
    return (state.currentPage - 1) * state.itemsPerPage
  },
  missionEnd: state => {
    return (state.currentPage * state.itemsPerPage) -1
  }
}

const actions = {
  async getMissions({ commit }) {
    const { data } = await axios.get('missions')
    commit('setMissions', data)
  },
  async closeAll({ commit }) {
    await commit('closeSideMenu')
    await commit('closeMissionFrom')
  }
}

const mutations = {
  setMissions(state, missions) {
    state.missions = missions
  },
  toggleMissionFrom(state) {
    state.createMission = !state.createMission
  },
  toggleSideMenu(state) {
    state.sideMenu = !state.sideMenu
  },

  closeMissionFrom(state) {
    state.createMission = false
  },
  closeSideMenu(state) {
    state.sideMenu = false
  }
}

const store = () => {
  return new Vuex.Store({
    strict: true,
    state,
    getters,
    mutations,
    actions
    // modules: {
    //   MissionList
    // }
  })
}

if (module.hot) {
  module.hot.accept([
    state,
    getters,
    mutations,
    actions
    // './modules/MissionList'
  ], () => {
    // const newModuleMission = require('./modules/MissionList').default
    store.hotUpdate({
      state: newState,
      getters: newGetters,
      mutations: newMutations,
      actions: newActions
      //   modules: {
      //     MissionList
      //   }
    })
  })
}

// const unsync = sync(store, router, {moduleName: "$route"})

export default store
