import axios from '~/plugins/axios'

// state
const state = {
  missions: []
}

// getter
const getters = {
  totalMissionCreated: state => {
    return state.missions.length
  }
}

// action
const actions = {
  async getMissions({
    commit
  }) {
    const {
      data
    } = await axios.get('missions')
    commit('setMissions', data)
  }
}

// mutatuion
const mutations = {
  setMissions(state, missions, totalMissionCreated) {
    state.missions = missions
  }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
