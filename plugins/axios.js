import axios from 'axios'

export default axios.create({
  baseURL: process.env.NODE_ENV === 'production' ? 'https://api.singlv.com' : 'http://localhost:5000'
  // baseURL: 'http://localhost:5000'
})
