const {resolve} = require('path')
// const axios = require('axios')

module.exports = {
  /*
   ** Build configuration
   */
  build: {
    // vendor: [
    //   'vee-validate'
    // ],
    postcss: {
      plugins: {
        'postcss-custom-properties': false
      }
    }

  },
  router: {
    middleware: 'closeAll',
    scrollBehavior(to, from, savedPosition) {
      return {
        x: 0,
        y: 0
      }
    }
  },
  /*
   ** Headers
   ** Common headers are already provided by @nuxtjs/pwa preset
   */
  head: {
    title: 'MHW 集會所',
    titleTemplate: '%s - LEVEL UP',
    link: [{
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Slabo+27px'
      }
    ],
    meta: [{
      name: 'viewport',
      content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'
    }, {
      charset: 'utf-8'
    }]
  },
  css: [
    'normalize.css',
    '~/assets/css/main.css'
  ],
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#3B8070'
  },
  /*
   ** Customize app manifest
   */
  manifest: {
    theme_color: '#3B8070'
  },
  /*
   ** Modules
   */
  modules: [
    ['nuxt-sass-resources-loader', resolve(__dirname, './assets/css/_common.scss')]
    // '@nuxtjs/pwa',
    // '@nuxtjs/axios'
  ]
  // plugins: [
  //   {src: '~/plugins/vee-validate', ssr: false},
  //   {src: '~/plugins/v-blur', ssr: false}
  // ]
  // generate: {
  //   routes: function() {
  //     return axios.get('https://jsonplaceholder.typicode.com/posts')
  //   }
  // }
}
